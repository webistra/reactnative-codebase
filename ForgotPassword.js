'use strict'

// React
import React from 'react'
import {View,
        Text,
        KeyboardAvoidingView,
        ImageBackground,
        Image,
        TouchableOpacity,
        Keyboard,
        TouchableWithoutFeedback,
        Modal
} from 'react-native'
// Navigation
import { addNavigationHelpers } from 'react-navigation'
// Redux
import { connect } from 'react-redux'
//import { TextField } from 'react-native-material-textfield'
import * as Progress from 'react-native-progress'

import { Button } from '../../SettingsTab/Templates/Button'
import {Button as Buttons} from '../../Assets/Button.js'
import {ReduxFormTextField} from '../../Assets/ReduxFormTextField.js'
import {RequiredValidation, EmailValidation} from '../../Assets/Validations.js'
import PropTypes from 'prop-types'
import {VALER_PURPLE} from '../../Assets/GlobalStyle'

//redux-form
import { Field, reduxForm, Form, SubmissionError, reset, blur, touch } from 'redux-form'

import {  forgotPasswordRequest } from '../../Actions';

  // import { loginUser } from '../AuthActions'

let background = require('../../Assets/Login/login-bg.png')
let logo = require('../../Assets/Login/logo.png')
let email = require('../../Assets/Login/email.png')
let password = require('../../Assets/Login/password.png')
let self;

class ForgotPassword extends React.Component {

  static navigationOptions = {
    title: 'Forgot Password',
    header: null,
    headerBackTitle: null,

  }
  state = {
    isLoading: false,
    isError: false,
    isSuccess: false,
    errorMessage: '',
    email: '',
    userId : ''
  }

  componentWillMount(){
     this.setState({ userId : this.props.user && this.prop.user.login_user ? this.prop.user.login_user : ''},() => {
       if(this.state.userId){
          this.props.navigation.navigate('Main');
       }
     })
  }
  onSubmitEditing (){

    // TODO:fix this
   // this.refs.password.getRenderedComponent().focus()
  }

  /*onEmailChange(text) {
    this.setState({email: text});
  }

  onPasswordChange(text) {
    this.setState({password: text});
  }*/

  onEmailChange(event,val) {
    self.setState({email:val});
  }

  static propTypes = {
    users: PropTypes.object,
     firebase: PropTypes.shape({
      push: PropTypes.func.isRequired
    })
  }

  onLoginSubmit (values){

    this.setState({isLoading: true});
    this.props.reset('forgotPassword');
    this.props.forgotPasswordRequest({email: values.Email});
  }

  componentDidMount(){
      self = this
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.forgotPasswordError) {
      this.setState({isLoading: false, isError: true, errorMessage: nextProps.forgotPasswordError, isSuccess: false});
    } else if(nextProps.forgotPasswordSuccess) {
      this.setState({isLoading: false, isError: false, errorMessage: '', isSuccess: true});
      // let navigate = this.props.navigation.navigate;
      // setTimeout(()=>{ navigate('Login'); }, 1000);
    }
  }

  render(){

    const {error, handleSubmit, navigationState, dispatch} = this.props
    return (

      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <KeyboardAvoidingView style={{flex:1}} behavior= 'padding'>
          <ImageBackground source={background} style={{flex:1}}>
            <Image source={logo} style={{alignSelf: 'center', marginTop: 25, height: 150, width: 250}} resizeMode= 'contain'/>
              <View style={{marginLeft: 15, marginRight: 15, marginVertical: 0}}>

                  <View style= {{flexDirection: 'row'}}>
                    <Image source={email} style={{marginTop: 18, marginRight: 8}}/>

                    <Field
                      name="Email"
                      label= 'Email'
                      component={ReduxFormTextField}
                      onSubmitEditing= {this.onSubmitEditing.bind(this)}
                      returnKeyType= {'next'}
                      secureTextEntry={false}
                      value={this.state.email}
                      onChange={this.onEmailChange}
                      //withRef={true}
                      validate={[RequiredValidation,EmailValidation]}
                    />

                  </View>
                
                <Buttons onPress={ this.props.handleSubmit(this.onLoginSubmit.bind(this))} style = {{marginTop: 14}}>Forgot Password</Buttons>

                <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.props.navigation.navigate('Login')}>
                  <Text style={{backgroundColor: 'transparent', color: 'white', marginTop: 50}}>Remember your password? Login</Text>
                </TouchableOpacity>

              </View>
          </ImageBackground>

          <Modal
              animationType="fade"
              transparent={true}
              visible={this.state.isLoading}
              onRequestClose={() => {alert("Modal has been closed.")}}
          >
              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <View style={{flexDirection: 'row', backgroundColor: 'black', borderRadius: 5, padding: 5}}>
                  <Text style={{color: '#ffffff', fontSize: 20}}> Please wait... &nbsp;</Text>
                  <Progress.Circle size={30} indeterminate={true} thickness={50} borderWidth={3}  color='white' showsText={true} />
                </View>
              </View>
          </Modal>

          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.isError}
            onRequestClose={() => {alert("Modal has been closed.")}}
          >
          <View style={{flex:1,backgroundColor:'rgba(0, 0, 0, 0.7)',justifyContent:'center',alignItems:'center'}}>
            <View style={{backgroundColor:'white', width:300, padding:20, borderRadius:5}}>

              <Text style={{textAlign:'center', margin:10, fontSize:18}}> Login Error</Text>
              <Text style={{textAlign:'center', margin:10, fontSize:15, color: 'grey'}}>"{this.state.errorMessage}" </Text>

              <TouchableOpacity onPress={()=>{
                  this.setState({isError: false})
              }} >
               <View style={{ marginTop: 20 }}>
                  <Button
                      Label= 'Try Again'
                      Style= {{backgroundColor:'red', color: 'white'}}
                  />
               </View>
              </TouchableOpacity>
            </View>
          </View>
          </Modal>

          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.isSuccess}
            onRequestClose={() => {alert("Modal has been closed.")}}
          >
          <View style={{flex:1,backgroundColor:'rgba(0, 0, 0, 0.7)',justifyContent:'center',alignItems:'center'}}>
            <View style={{backgroundColor:'white', width:300, padding:20, borderRadius:5}}>

              <Text style={{textAlign:'center', margin:10, fontSize:18}}> Success</Text>
              <Text style={{textAlign:'center', margin:10, fontSize:15, color: 'grey'}}>We have sent an email to your registered email. Please check your inbox to reset your password</Text>

              <TouchableOpacity onPress={()=>{
                  this.setState({isSuccess: false})
                  this.props.navigation.navigate('Login')
              }} >
               <View style={{ marginTop: 20 }}>
                  <Button
                      Label= 'Ok'
                      Style= {{backgroundColor:'green', color: 'white'}}
                  />
               </View>
              </TouchableOpacity>
            </View>
          </View>
          </Modal>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    )
  }
}

ForgotPassword = reduxForm({
  // a unique name for the form
  form: 'forgotPassword'
})(ForgotPassword)

const mapStateToProps = ({state,auth}) => {
  return {
    user : auth.user
  };
}

export default connect(mapStateToProps, {forgotPasswordRequest, reset})(ForgotPassword);
